package com.example.company.myeyes.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.company.myeyes.GPSTracker;
import com.example.company.myeyes.R;

import java.util.ArrayList;

/**
 * Created by company on 05/01/2016.
 */
public class HomeFragment extends Fragment implements TextToSpeech.OnInitListener, LocationListener {

    private TextToSpeech objetoTextoaVoz;
    public static String resultado = "";
    public static String destino = "";
    public boolean validar = false;
    protected static final int RESULT_SPEECH = 1;
    private Button buttonTalk;
    private Button buttonGps;

    private LocationManager locationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        objetoTextoaVoz = new TextToSpeech(getActivity(), this);


        buttonTalk = (Button) view.findViewById(R.id.buttonTalk);
        buttonGps = (Button) view.findViewById(R.id.buttonGps);

        buttonTalk.setOnClickListener(talkButtonListener);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);

            }
        } else {
            configureButton();
        }


        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    configureButton();
                }
                return;
        }
    }

    private void configureButton() {
        buttonGps.setOnClickListener(gpsButtonListener);

    }

    /**
     *
     */
    private void clicGpsButton(){

//        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            requestPermissions(new String[]{
//                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
//            }, 10);
//        }
//        locationManager.requestLocationUpdates("gps", 5000, 0, this);
//        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        onLocationChanged(location);

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        double lat = gpsTracker.latitude;
        double lon = gpsTracker.longitude;
    }

    /**
     *
     */
    private View.OnClickListener gpsButtonListener = new View.OnClickListener() {
        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onClick(View v) {
            Log.e(HomeFragment.class.getName(), "CLICK!!");
            clicGpsButton();

        }
    };

    /**
     *
     */
    private View.OnClickListener talkButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            grabar();
        }
    };


    private void speakOut(String mensaje, boolean accion) {

        // String text = TextOut.getText().toString();

        objetoTextoaVoz.speak(mensaje, TextToSpeech.QUEUE_FLUSH, null);
        while (objetoTextoaVoz.isSpeaking()) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (accion) {
            String destino = resultado;
            grabar();
        }

    }

    public void grabar() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-LA");

        try {
            startActivityForResult(intent, RESULT_SPEECH);

        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(getActivity(),
                    "Error capturando voz",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        resultado = "";
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == -1 && null != data) {

                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    resultado = text.get(0);

                    if (!validar) {
                        validar = true;
                        speakOut("Nuestro destino es: " + resultado + ", es correcto?", validar);
                    } else {
                        if (resultado.toUpperCase().contains("SI") || resultado.toUpperCase().contains("CORRECTO")) {
                            //enviarUbicaciones();
                            Toast.makeText(getActivity(), "Enter to result speech", Toast.LENGTH_LONG);
                        } else {
                            validar = false;
                            speakOut("Repita nuestro destino", validar);
                            grabar();
                        }
                    }

                }
                break;
            }

        }


    }

    @Override
    public void onInit(int status) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(HomeFragment.class.getName(), "\n  " + location.getLatitude() + " " + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }
}
